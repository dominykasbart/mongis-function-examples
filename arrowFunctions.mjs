const myFunction = () => {
  console.log("Hello, its me, a function!");
};

function addNumbersBuilder(num1) {
  return (num2) => {
    return num1 + num2;
  };
}

const fn1 = addNumbersBuilder(9);
const result = fn1(2);

console.log("Result:", result);
