/**
 * Takes an argument and returns a string value
 * @param number
 */
export function getEmail(number) {
  return `mongirdas+${number}@reneza.com`;
}

/**
 * I take no argument and return nothing
 */
export function noReturn() {
  console.log("I return nothing");
}

/**
 * Takes two numbers and adds them together
 * @param num1
 * @param num2
 */
export function addNumbers(num1, num2) {
  return num1 + num2;
}

/**
 * I take an argument and do not return anything
 * @param data
 */
export function logData(data) {
  console.log("Data:", data);
}

export function letExample() {
  let x = 2;
  console.log("X:", x);
  x = 3;
  console.log("X:", x);
}
